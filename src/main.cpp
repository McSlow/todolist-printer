#include <Arduino.h>
#include <WiFi.h>
#include "secrets.h"
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include "time.h"
#include "Adafruit_Thermal.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/FreeSans9pt7b.h>
// #include "cart.h"


//WiFi
const char* ssid = WIFINET;
const char* password =  WIFIPASS;

//NTP Stuff
const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 7200;
const int   daylightOffset_sec = 3600;
 
//Todoist API: https://developer.todoist.com/rest/v1/#tasks
String serverName = "https://api.todoist.com/rest/v1/tasks?project_id=";
String serverPath = serverName + TODOISTLIST;

//JSON Stuff
String listEntries;
JSONVar myObject;

//Serial connection to the Printer
#define RXD2 16 // 2nd Hardware Serial, connected to the Thermal Printers TX Pin
#define TXD2 17 // 2nd Hardware Serial, connected to the Thermal Printers RX Pin

Adafruit_Thermal printer(&Serial2);     // Pass addr to printer constructor
//display
#define SCREEN_ADDRESS 0x3C
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

void displayMessage(const char* message){
  display.setCursor(0,13);
  display.println(message);
  display.display();
  display.clearDisplay();

}

String httpGETRequest(const char* serverName){
  HTTPClient http;

  // Your IP address with path or Domain name with URL path 
  http.begin(serverName);
  http.addHeader("Authorization", TODOISTOKEN);

  // Send HTTP POST request
  int httpResponseCode = http.GET();

  String payload = "{}"; 

  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    displayMessage("Todoist: " + httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
    displayMessage("http err " + httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

void setup() {
  //connect to wifi
  Serial.begin(9600);
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setFont(&FreeSans9pt7b);
  displayMessage("Connecing Wifi.");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
    displayMessage("Connecing Wifi.");
    //TODO make this abort after some time..
  }
  Serial.println("Connected to the WiFi network");
  displayMessage("Connected!");
  //get ntp time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    displayMessage("No NTP!");
    return;
  }

  //web request
  if(WiFi.status()== WL_CONNECTED){
              
      listEntries = httpGETRequest(serverPath.c_str());
      myObject = JSON.parse(listEntries);
  
      if (JSON.typeof(myObject) == "undefined") {
        Serial.println("Parsing input failed!");
        displayMessage("JSON Err!");
        return;
      }
    }
    else {
      Serial.println("WiFi Disconnected");
    }

  Serial2.begin(19200, SERIAL_8N1, RXD2, TXD2);
  //mySerial.begin(19200);  // Initialize SoftwareSerial
  //Serial1.begin(19200); // Use this instead if using hardware serial
  printer.begin();        // Init printer (same regardless of serial type)
  if(printer.hasPaper()){
    displayMessage("Printing");
    printer.setCharset(2);  // German special chars.
    printer.feed(1);
    printer.justify('C');
    // printer.printBitmap(cart_width, cart_height, cart_data);
    // printer.feed(1);
    printer.doubleHeightOn();
    printer.doubleWidthOn();
    printer.print(F("Einkaufen\n"));
    printer.doubleHeightOff();
    printer.doubleWidthOff();
    printer.println(&timeinfo, "Datum: %d. %B %Y");
    printer.feed(1);
    printer.justify('L');
    printer.setLineHeight(28);
    for(int entry = 0; entry < myObject.length(); entry++){
      String item = JSON.stringify(myObject[entry]["content"]);
      item.replace("\"", ""); //remove json quotes
      // see https://cdn-shop.adafruit.com/datasheets/CSN-A2+User+Manual.pdf , page 69
      item.replace("ü", "}");
      item.replace("Ü", "]");
      item.replace("ä", "{");
      item.replace("Ä", "[");
      item.replace("ö", "|");
      item.replace("Ö", "\\");
      item.replace("ß", "~");
      printer.println("- " + item);
    }
    printer.feed(5);
    displayMessage("Done!");
  }
  else{
    Serial.println("No Paper!");
    displayMessage("No Paper");
  }
  sleep(10);
  WiFi.disconnect();
}

void loop() {
  // put your main code here, to run repeatedly:
}